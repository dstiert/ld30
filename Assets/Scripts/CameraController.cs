﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	public GameObject Player;
	public ShipController Ship;
	public GameObject Dust;
	public float FlySize;
	public float BuildSize;

	private float targetSize;
	private Vector3 targetScale;
	private Vector3 baseScale;

	void Start()
	{
		Ship = Player.GetComponent<ShipController>();
		targetSize = FlySize;
		this.GetComponent<Camera>().orthographicSize = FlySize;
		targetScale = baseScale = Dust.transform.localScale;
	}

	void Update () 
	{
		this.transform.position = new Vector3 (Player.transform.position.x, Player.transform.position.y, -50f);
		Dust.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(-Player.transform.position.x / 100f, -Player.transform.position.y / 100f));

		targetSize = Ship.Building ? BuildSize : FlySize;

		if (Ship.Building) 
		{
			targetSize = BuildSize;
			targetScale = BuildSize/FlySize * baseScale;
		}
		else 
		{
			targetSize = FlySize;
			targetScale = baseScale;
		}

		this.GetComponent<Camera>().orthographicSize = Mathf.Lerp (this.GetComponent<Camera>().orthographicSize, targetSize, Time.deltaTime);
		Dust.transform.localScale = Vector3.Lerp(Dust.transform.localScale, targetScale, Time.deltaTime);

	}
}
