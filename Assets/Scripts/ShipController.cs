﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

	private GameObject[] Planets;
	public GameObject CurrentPlanet;
	public float GravityPow;
	public bool Building;
	public int AntennaCount;
	public float FuelAmount;

	float targetVolume;

	public bool Landed
	{
		get { return CurrentPlanet != null; }
	}

	void Start () 
	{
		Planets = GameObject.FindGameObjectsWithTag ("Planet");
		targetVolume = 0f;
	}

	void Update()
	{
		var audio = this.GetComponent<AudioSource>();
		audio.volume = Mathf.Lerp (audio.volume, targetVolume, 5 * Time.deltaTime);
		
		if (Landed && Input.GetKeyDown(KeyCode.B)) 
		{
			if(Building)
			{
				CurrentPlanet.GetComponent<PlanetController>().FinalizeBuild();
				Building = false;
			}
			else if(AntennaCount > 0)
			{
				AntennaCount--;
				Building = true;
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		if (!Building) 
		{
			if(FuelAmount > 0 && Input.GetAxis("Vertical") != 0)
			{
				FuelAmount -= Time.deltaTime;
				this.GetComponent<Rigidbody>().AddForce(this.transform.up * Input.GetAxis("Vertical") * 140);
				targetVolume = 1;
			}
			else
			{
				targetVolume = 0;
			}

			this.GetComponent<Rigidbody>().AddTorque(Vector3.back * Input.GetAxis ("Horizontal") * 150);
		}

		foreach (var planet in Planets) 
		{
			Vector3 r = (this.GetComponent<Rigidbody>().position - planet.transform.position);
			Vector3 f = -(500f/Mathf.Pow(r.magnitude, GravityPow)) * r.normalized;
			this.GetComponent<Rigidbody>().AddForce(f);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		CurrentPlanet = collision.gameObject;
	}

	void OnCollisionExit(Collision collision)
	{
		CurrentPlanet = null;
	}
}
