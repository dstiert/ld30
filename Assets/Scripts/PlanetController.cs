﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlanetController : MonoBehaviour {

	float Dir;
	float Length;
	float Arc;
	float targetVolume;

	ShipController Ship;
	IList<PlanetController> Neighbors;
	public GameObject Player;
	public Material AntennaBuild;
	public Material AntennaDone;
	public bool Origin = false;
	public bool Probe;

	private GameObject ActiveChild;

	public bool Connected()
	{
		return this.Connected(new List<PlanetController>{this});
	}

	public bool Connected(IList<PlanetController> exclude)
	{
		exclude.Add (this);
		return this.Origin || this.Probe || (Neighbors != null && Neighbors.Any(p => !exclude.Contains(p) && p.Connected(exclude)));
	}

	public void FinalizeBuild()
	{
		this.ActiveChild.GetComponent<LineRenderer>().material = AntennaDone;
		this.ActiveChild = null;
		this.GetComponents<AudioSource>()[1].Play();
		foreach (var planet in ProbeArea(Dir, Length, Arc)) 
		{
			Neighbors.Add(planet);
			planet.Neighbors.Add(this);
		}
	}
	
	void Start () 
	{
		Dir = 0f;
		Length = 10f;
		Arc = 45f;
		Ship = Player.GetComponent<ShipController> ();
		Neighbors = new List<PlanetController>();
		Probe = false;
		ActiveChild = null;
		targetVolume = 0f;
		this.transform.Rotate (new Vector3 (0f, Random.Range(0f, 360f), 0f));
	}
	
	// Update is called once per frame
	void Update () 
	{
		(this.GetComponent("Halo") as Behaviour).enabled = Connected();
		Probe = false;

		if (Ship.Landed && Ship.CurrentPlanet == this.gameObject && Ship.Building) 
		{
			if(Input.GetAxis ("Horizontal") != 0f || Input.GetAxis ("Vertical") != 0f)
			{
				targetVolume = 1f;
			}
			else
			{
				targetVolume = 0f;
			}

			Dir += Input.GetAxis ("Horizontal");
			Length += Input.GetAxis ("Vertical");
			Arc = Mathf.Lerp(0f, 360f, (100f - Length)/100f);

			if (Length < GetRadius() + 1f) 
			{
				Length = GetRadius() + 1f;
			} 
		 	else if (Length > 100f) 
			{
				Length = 100f;
			}

			this.ProbeArea(Dir, Length, Arc);
			this.SetLine (this.CreateLine (Dir, Length, Arc), 0.25f);
		}
		else
		{
			targetVolume = 0f;
		}
		
		var audio = this.GetComponents<AudioSource>()[0];
		audio.volume = Mathf.Lerp (audio.volume, targetVolume, 20 * Time.deltaTime);
	}

	IList<PlanetController> ProbeArea(float dir, float length, float arc)
	{
		var planets = GameObject.FindGameObjectsWithTag ("Planet")
			.Select (p => p.GetComponent<PlanetController> ())
				.Where(p => p != this && this.InRange(p, dir, length, arc)).ToList();

		foreach (var planet in planets)
		{
			planet.Probe = true;
		}
		return planets;
	}

	bool InRange(PlanetController p, float dir, float length, float arc)
	{
		var diff = p.transform.position - this.transform.position;
		if (diff.magnitude > length) 
		{
			return false;		
		}
		var angle = Vector3.Angle (Quaternion.AngleAxis(dir, Vector3.back) * Vector3.down, diff);

		if (angle < arc / 2f) 
		{
			RaycastHit hit;
			if(Physics.Raycast(new Ray(this.transform.position, (p.transform.position - this.transform.position)), out hit))
			{
				return hit.collider.gameObject == p.gameObject;
			}
   		}
		return false;
	}

	IList<Vector3> CreateLine(float dir, float length, float arc)
	{
		var v = new List<Vector3>();

		var start = dir - arc / 2f;
		var end = dir + arc / 2f;

		SweepRange (v, start, dir, length, 10);
		SweepRange (v, dir, end, length, 10);

		v.Add(this.transform.position + GetRadius() * (Quaternion.AngleAxis(end, Vector3.back) * Vector3.down));

		return v;
	}

	void SweepRange(IList<Vector3> v, float start, float end, float length, int count)
	{
		for(int i = 0; i < count; i++)
		{
			var v1 = this.transform.position + GetRadius() * (Quaternion.AngleAxis(Mathf.LerpAngle(start, end, i/(float)count), Vector3.back) * Vector3.down);
			var v2 = this.transform.position + length * (Quaternion.AngleAxis(Mathf.LerpAngle(start, end, (i + 0.5f)/(float)count), Vector3.back) * Vector3.down);
			v.Add(v1);
			RaycastHit hit;
			if(Physics.Raycast(new Ray(v1, (v2-v1).normalized), out hit, length))
			{
				v.Add(hit.point);
			}
			else
			{
				v.Add(v2);
			}
		}
	}

	float GetRadius()
	{
		return this.transform.localScale.x / 2f;
	}

	void SetLine(IList<Vector3> v, float width)
	{
		if (this.ActiveChild == null) 
		{
			this.ActiveChild = new GameObject();
			this.ActiveChild.AddComponent<LineRenderer>();
			this.ActiveChild.gameObject.transform.parent = this.gameObject.transform;
		}
		var line = this.ActiveChild.GetComponent<LineRenderer>();
		line.material = AntennaBuild;
		line.SetVertexCount(v.Count * 2 - 1);
		line.SetWidth (width, width);
		int c = 1;

		line.SetPosition(0, v[0]);

		for(int i = 1; i < v.Count; i++)
		{
			line.SetPosition(c++, v[i]);
			line.SetPosition(c++, v[i] + 0.1f * (v[i] - v[i-1]).normalized); 
		}
	}
}
