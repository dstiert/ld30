﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {

	private IList<PlanetController> Planets;
	public int LevelNum;
	public ShipController Ship;
	public GUIText MessageText;
	public GUIText AntennaCount;
	public GUITexture FuelBar;
	public int StartAntennaCount;
	public float StartFuelAmount;
	private float fuelWidth, fuelHeight;
	private bool done;
	
	void Start () 
	{
		Planets = GameObject.FindGameObjectsWithTag("Planet").Select(p => p.GetComponent<PlanetController>()).ToList();
		Ship.AntennaCount = StartAntennaCount;
		Ship.FuelAmount = StartFuelAmount;
		this.fuelWidth = FuelBar.pixelInset.width;
		this.fuelHeight = FuelBar.pixelInset.height;
		done = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		AntennaCount.text = Ship.AntennaCount.ToString ();
		FuelBar.pixelInset = new Rect(0,0, fuelWidth * (Ship.FuelAmount / this.StartFuelAmount), this.fuelHeight);

		if(Ship.transform.position.magnitude > 150f)
		{
			MessageText.enabled = true;
			MessageText.text = "Lost?\nPress R to reset.";
		}	
		
		if (Ship.FuelAmount < 0) 
		{
			MessageText.enabled = true;
			MessageText.text = "No Fuel?\nPress R to reset.";
		}

		if (Ship.AntennaCount == 0 && !Ship.Building) 
		{
			MessageText.enabled = true;
			MessageText.text = "No Antenna?\nPress R to reset.";
		}
		
		if(Planets.All(p => p.Connected() && !p.Probe))
		{
			MessageText.enabled = true;
			MessageText.text = this.LevelNum == -1 ? "Your job is complete!\nFinal Score: " + Score.TotalScore : "Success!\nPress N to continue.";

			if(!done)
			{
				Score.TotalScore += (int)(Ship.FuelAmount * 10f);
				done = true;
				this.GetComponents<AudioSource>()[0].PlayDelayed(1f);
			}
		}

		if(done && Input.GetKeyDown(KeyCode.N) && this.LevelNum != -1)
		{
			Application.LoadLevel(string.Format("Level{0}", this.LevelNum + 1));
		}

		if(Input.GetKeyDown(KeyCode.R))
		{
			Application.LoadLevel(Application.loadedLevelName);
		}
	}
}
