﻿using UnityEngine;
using System.Collections;

public class TutorialTrigger : MonoBehaviour {

	public string TutorialText;
	public GUIText MessageText;

	void Start()
	{
		TutorialText = TutorialText.Replace("\\n", "\n");
	}

	void OnTriggerEnter(Collider other)
	{
		MessageText.enabled = true;
		MessageText.text = TutorialText;
	}

	void OnTriggerExit(Collider other)
	{
		MessageText.enabled = true;
		MessageText.text = "";
	}
}
